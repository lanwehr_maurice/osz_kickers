package osz_kickers;

public class EhrenamtlicherSchiedsrichter extends Mitglied {

	private int anzahlGepffifeneSpiele;

	public EhrenamtlicherSchiedsrichter(String name, String telefonnummer, boolean jahresbeitragBeglichen,
			int anzahlGepffifeneSpiele) {
		super(name, telefonnummer, jahresbeitragBeglichen);
		this.anzahlGepffifeneSpiele = anzahlGepffifeneSpiele;
	}

	public int getAnzahlGepffifeneSpiele() {
		return anzahlGepffifeneSpiele;
	}

	public void setAnzahlGepffifeneSpiele(int anzahlGepffifeneSpiele) {
		this.anzahlGepffifeneSpiele = anzahlGepffifeneSpiele;
	}

}
