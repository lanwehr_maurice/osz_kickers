package osz_kickers;

public class Spieler extends Mitglied {

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public Spieler(String name, String telefonnummer, boolean jahresbeitragBeglichen, int trikotnummer,
			String position) {
		super(name, telefonnummer, jahresbeitragBeglichen);
		this.trikotnummer = trikotnummer;
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	private int trikotnummer;
	private String position;

}
