package osz_kickers;

public class Trainer extends Mitglied {

	public Trainer(String name, String telefonnummer, boolean jahresbeitragBeglichen, String lizensklasse,
			int monatlicheAufwandsentschaedigung) {
		super(name, telefonnummer, jahresbeitragBeglichen);
		this.lizensklasse = lizensklasse;
		this.monatlicheAufwandsentschaedigung = monatlicheAufwandsentschaedigung;
	}

	private String lizensklasse;

	public String getLizensklasse() {
		return lizensklasse;
	}

	public void setLizensklasse(String lizensklasse) {
		this.lizensklasse = lizensklasse;
	}

	public int getMonatlicheAufwandsentschaedigung() {
		return monatlicheAufwandsentschaedigung;
	}

	public void setMonatlicheAufwandsentschaedigung(int monatlicheAufwandsentschaedigung) {
		this.monatlicheAufwandsentschaedigung = monatlicheAufwandsentschaedigung;
	}

	private int monatlicheAufwandsentschaedigung;
}
