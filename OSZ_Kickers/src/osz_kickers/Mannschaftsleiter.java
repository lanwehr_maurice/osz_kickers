package osz_kickers;

public class Mannschaftsleiter extends Mitglied {

	private String mannschaftsname;

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public String getRabattJahresbeitrag() {
		return rabattJahresbeitrag;
	}

	public void setRabattJahresbeitrag(String rabattJahresbeitrag) {
		this.rabattJahresbeitrag = rabattJahresbeitrag;
	}

	private String rabattJahresbeitrag;

	public Mannschaftsleiter(String name, String telefonnummer, boolean jahresbeitragBeglichen, String mannschaftsname,
			String rabattJahresbeitrag) {
		super(name, telefonnummer, jahresbeitragBeglichen);
		this.mannschaftsname = mannschaftsname;
		this.rabattJahresbeitrag = rabattJahresbeitrag;
	}

}
