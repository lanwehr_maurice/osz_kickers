package osz_kickers;

public abstract class Mitglied {

private String name; 
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getTelefonnummer() {
	return telefonnummer;
}
public Mitglied(String name, String telefonnummer, boolean jahresbeitragBeglichen) {
	super();
	this.name = name;
	this.telefonnummer = telefonnummer;
	this.jahresbeitragBeglichen = jahresbeitragBeglichen;
}
public void setTelefonnummer(String telefonnummer) {
	this.telefonnummer = telefonnummer;
}
public boolean isJahresbeitragBeglichen() {
	return jahresbeitragBeglichen;
}
public void setJahresbeitragBeglichen(boolean jahresbeitragBeglichen) {
	this.jahresbeitragBeglichen = jahresbeitragBeglichen;
}
private String telefonnummer;
private boolean jahresbeitragBeglichen; 

}
